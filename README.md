# rnWorks

Pilot project to create simple react native architecture.


# Navigation

There are several methods to navigate between screens

`pushToMain` method will bring to the top of navigation. 

    import { pushToMain, HOME } from "@navigations"
    
    const page = props => {
	    pushTo(HOME);
    }
    

`push` method will bring to a new screen. By default, it will have back button and listen native back button as well from the device. 

    import { pushTo, LIST_SCREEN } from "@navigations"
    
    const page = props => {
	    pushTo(props.componentId, LIST_SCREEN);
    }
    

`backToPrevious` method will navigate back to the previous screen

    import { backToPrevious } from "@navigations"
    
    const page = props => {
	    backToPrevious();
    }

`showModal` method will show a screen as a modal. It will trigger the screen as open and close rather than forward and back. Anyway, when the modal is open, by pressing back it will close the modal. `dismissModalAll` method will close the modal

    import { showModal, dismissModalAll, GALLERY_SCREEN } from  "@navigations"
    
    // to open
    showModal(GALLERY_SCREEN);
    
	// to dismiss
    dismissModalAll();


# Redux

Sample of fetching and retrieving data with react hooks

    import { fetchStudents, getStudents } from "@redux/student"
    import { useDispatch, useSelector } from "react-redux"
    import { useEffect } from "react";

	const studentScreen = props => {
		const  dispatch  =  useDispatch();
		
		// to fetch data on initial (equals to componentDidMount)
		useEffect(() => {
			dispatch(fetchStudents());
		}, []);

		// to retrieve the data
		const { list } =  useSelector(getStudents);
	}
