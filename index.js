import { Navigation } from "react-native-navigation";
import { pushToMainScreen } from "@navigations";
import registerScreens from "@navigations/registerScreens";

registerScreens();

Navigation.events().registerAppLaunchedListener(() => {
  pushToMainScreen();
});
