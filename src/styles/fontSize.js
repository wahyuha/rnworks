import { Dimensions, Platform, PixelRatio } from "react-native";

const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("window");

const scale = SCREEN_WIDTH / 375;

const normalize = size => {
  const newSize = size * scale;
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
};

const fontSize = {
  MINI: {
    fontSize: normalize(8)
  },
  SMALL: {
    fontSize: normalize(10)
  },
  MEDIUM: {
    fontSize: normalize(12)
  },
  LARGE: {
    fontSize: normalize(18)
  },
  XLARGE: {
    fontSize: normalize(20)
  }
};

const fontWeight = {
  REGULAR: {
    fontFamily: "Montserrat-Regular"
  },
  MEDIUM: {
    fontFamily: "Montserrat-Medium"
  },
  SEMIBOLD: {
    fontFamily: "Montserrat-SemiBold"
  },
  BOLD: {
    fontFamily: "Montserrat-Bold"
  },
  EXTRABOLD: {
    fontFamily: "Montserrat-ExtraBold"
  }
};

export { fontSize, fontWeight };
