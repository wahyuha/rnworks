/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { SafeAreaView, ScrollView, Text } from "react-native";
import { Search } from "@components";

const SearchScreen = props => {
  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <Search {...props} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SearchScreen;
