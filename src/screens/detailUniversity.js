/* eslint-disable quotes */
import React, { useEffect } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { DetailUniversity } from "@components";
import {
  fetchUniversityDetail,
  getUniversityDetail
} from "@redux/universityDetail";
import { useDispatch, useSelector } from "react-redux";

const Detail = props => {
  let { detail } = props;
  const { append } = props;
  const dispatch = useDispatch();
  useEffect(() => {
    if (append) {
      dispatch(fetchUniversityDetail(detail.id));
    }
  }, []);

  const data = useSelector(getUniversityDetail);
  const { isLoaded } = data;
  if (append && isLoaded) {
    detail = data.detail;
  }

  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <DetailUniversity {...props} detail={detail} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Detail;
