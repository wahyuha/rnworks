/* eslint-disable quotes */
import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { Home } from "@components";

const homeComponent = props => {
  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <Home {...props} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default homeComponent;
