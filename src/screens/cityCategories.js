/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable quotes */
import _ from "lodash";
import React, { useEffect } from "react";
import { ActivityIndicator, SafeAreaView, ScrollView } from "react-native";
import { CityCategories } from "@components";
import { fetchCities, getCities } from "@redux/city";
import { useDispatch, useSelector } from "react-redux";

const cityCategories = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCities());
  }, []);

  const { isFetching, isLoaded, isFailed, list } = useSelector(getCities);
  const citiesProps = {
    ...props,
    cities: list,
    isFailed,
    isFetching,
    isLoaded
  };

  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <CityCategories {...citiesProps} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default cityCategories;
