/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable quotes */
import React, { useEffect } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";
import { pushTo } from "@navigations";
import { CITY_CATEGORIES } from "@navigations/screens";
import { ListUniversity, SearchBar } from "@components";
import { fetchUniversities, getUniversities } from "@redux/university";
import { useDispatch, useSelector } from "react-redux";

const listing = props => {
  const { city } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUniversities({ city }));
  }, []);

  const cityRender = () => {
    if (city) {
      return (
        <TouchableHighlight
          underlayColor="white"
          onPress={() => pushTo(props.componentId, CITY_CATEGORIES)}
        >
          <View style={styles.cityWrap}>
            <View style={styles.cityIcon}>
              <Image
                source={require("@icons/marker.png")}
                style={{ width: 16, height: 20 }}
              />
            </View>
            <Text style={styles.cityLabel}>{city.name}</Text>
          </View>
        </TouchableHighlight>
      );
    }
    return null;
  };

  const { isFetching, isLoaded, isFailed, list } = useSelector(getUniversities);
  const listProps = {
    ...props,
    isFailed,
    isFetching,
    isLoaded
  };
  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <SearchBar />
          {cityRender()}
          <ListUniversity {...listProps} list={list} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  cityWrap: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    flexDirection: "row",
    alignItems: "center"
  },
  cityIcon: {
    padding: 4,
    marginRight: 8
  },
  cityLabel: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD
  }
});

export default listing;
