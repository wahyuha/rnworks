/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { NewsDetail } from "@components";

const News = props => {
  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <NewsDetail {...props} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default News;
