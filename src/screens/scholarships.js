/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { ScholarshipDetail } from "@components";

const Scholarships = props => {
  return (
    <>
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <ScholarshipDetail {...props} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Scholarships;
