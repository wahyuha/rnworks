import { FETCH_ARTICLE } from "./constants";

export const fetchArticleAsync = ({ type, articleId }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: `posts/${articleId}`
      // params: {}
    }
  }
});

export const fetchArticle = articleId => dispatch => {
  return dispatch(
    fetchArticleAsync({
      type: FETCH_ARTICLE,
      articleId
    })
  );
};
