export const getArticle = state => {
  return state.article;
  /* TODO: mana yang lebih mudah,
      langsung return semua state article
      atau state loadernya dipisah menggunakan getArticleLoadingStatus?
  */
  if (!state.article.isLoaded) {
    return [];
  }

  return state.article.data;
};

export const getArticleLoadingStatus = state => state.article.isFetching;
