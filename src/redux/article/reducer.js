import { FETCH_ARTICLE } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  data: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const article = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTICLE:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_ARTICLE):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        data: action.payload.data
      };
    case failure(FETCH_ARTICLE):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default article;
