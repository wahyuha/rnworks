import { FETCH_UNIVERSITY_DETAIL } from "./constants";

export const fetchUniversityDetail = id => dispatch => {
  return dispatch({
    type: FETCH_UNIVERSITY_DETAIL,
    payload: {
      request: {
        method: "get",
        url: `universities/${id}`
      }
    }
  });
};
