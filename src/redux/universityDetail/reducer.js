import { FETCH_UNIVERSITY_DETAIL } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  detail: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const university = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_UNIVERSITY_DETAIL:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_UNIVERSITY_DETAIL):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        detail: action.payload.data
      };
    case failure(FETCH_UNIVERSITY_DETAIL):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default university;
