import { FETCH_UNIVERSITY } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  featured: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const university = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_UNIVERSITY:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_UNIVERSITY):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        featured: action.payload.data[0]
          ? action.payload.data[0].universities
          : []
      };
    case failure(FETCH_UNIVERSITY):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default university;
