import { FETCH_UNIVERSITY } from "./constants";

export const fetcFeaturedUniversitiesAsync = ({ type }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: "topuniversities"
    }
  }
});

export const fetchFeaturedUniversities = () => dispatch => {
  return dispatch(
    fetcFeaturedUniversitiesAsync({
      type: FETCH_UNIVERSITY
    })
  );
};
