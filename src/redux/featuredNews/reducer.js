import { FETCH_NEWS } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  featured: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const news = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_NEWS):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        featured: action.payload.data[0].news
      };
    case failure(FETCH_NEWS):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default news;
