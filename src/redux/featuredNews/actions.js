import { FETCH_NEWS } from "./constants";

export const fetcFeaturedNewsAsync = ({ type }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: "topnews"
    }
  }
});

export const fetchFeaturedNews = () => dispatch => {
  return dispatch(
    fetcFeaturedNewsAsync({
      type: FETCH_NEWS
    })
  );
};
