import { combineReducers } from "redux";
import articleReducer from "./article/reducer";
import cityReducer from "./city/reducer";
import featuredNewsReducer from "./featuredNews/reducer";
import featuredUniversitiesReducer from "./featuredUniversities/reducer";
import featuredScholarshipsReducer from "./featuredScholarships/reducer";
import universityReducer from "./university/reducer";
import universityDetailReducer from "./universityDetail/reducer";
import userReducer from "./user/reducer";
import searchReducer from "./search/reducer";

export default combineReducers({
  article: articleReducer,
  city: cityReducer,
  featuredNews: featuredNewsReducer,
  featuredUniversities: featuredUniversitiesReducer,
  featuredScholarships: featuredScholarshipsReducer,
  university: universityReducer,
  universityDetail: universityDetailReducer,
  user: userReducer,
  search: searchReducer
});
