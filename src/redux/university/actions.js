import { FETCH_UNIVERSITY } from "./constants";

export const fetchUniversitiesAsync = ({ type, id }) => {
  const payload = id ? `city.id_eq=${id}&` : "";
  return {
    type,
    payload: {
      request: {
        method: "get",
        url: `universities?${payload}_sort=name:ASC`
      }
    }
  };
};

export const fetchUniversities = ({ city }) => dispatch => {
  return dispatch(
    fetchUniversitiesAsync({
      type: FETCH_UNIVERSITY,
      id: city ? city.id : null
    })
  );
};
