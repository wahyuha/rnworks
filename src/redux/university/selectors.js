export const getUniversities = state => {
  return state.university;
  if (!state.university.isLoaded) {
    return [];
  }

  return state.university.list;
};

export const getUniversitiesLoadingStatus = state => state.university.isFetching;
