import { FETCH_REGISTER, FETCH_USER } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  registered: false,
  user: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REGISTER:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_REGISTER):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        registered: true
      };
    case failure(FETCH_REGISTER):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    case FETCH_USER:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_USER):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        user: action.payload.data
      };
    case failure(FETCH_USER):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default user;
