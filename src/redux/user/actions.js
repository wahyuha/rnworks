import { AsyncStorage } from "react-native";
import { FETCH_REGISTER, FETCH_USER } from "./constants";

const postRegisterAsync = ({ type, payload }) => {
  const params = {
    name: payload.name,
    no_wa: payload.phone,
    gender: payload.gender,
    country: "Indonesia", // TODO:
    province: payload.province,
    year_of_birth: payload.birtDate
  };
  return {
    type,
    payload: {
      request: {
        method: "post",
        url: "register",
        data: params
      }
    }
  };
};

export const fetchUser = userId => dispatch => {
  return dispatch({
    type: FETCH_USER,
    payload: {
      request: {
        method: "get",
        url: `member/${userId}`
      }
    }
  });
};

export const postRegister = payload => dispatch => {
  return dispatch(
    postRegisterAsync({
      type: FETCH_REGISTER,
      payload
    })
  )
    .then(response => {
      // dispatch(fetchUser(3)).then(response => saveUser(response));
    })
    .catch(error => console.log(error));
};

const saveUser = action => {
  const { data } = action.payload.data;
  AsyncStorage.setItem("@storage_ui", data.id);
  AsyncStorage.setItem("@storage_un", data.name);
};
