export const getUser = state => {
  if (!state.user.isLoaded) {
    return [];
  }

  return state.user.data;
};

export const getUserLoadingStatus = state => state.user.isFetching;
