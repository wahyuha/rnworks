import { FETCH_SCHOLARSHIP } from "./constants";

export const fetcFeaturedScholarshipsAsync = ({ type }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: "topscholarships"
    }
  }
});

export const fetchFeaturedScholarships = () => dispatch => {
  return dispatch(
    fetcFeaturedScholarshipsAsync({
      type: FETCH_SCHOLARSHIP
    })
  );
};
