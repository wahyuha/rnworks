import { FETCH_SCHOLARSHIP } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  featured: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const scholarships = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SCHOLARSHIP:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_SCHOLARSHIP):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        featured: action.payload.data[0].scholarships
      };
    case failure(FETCH_SCHOLARSHIP):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default scholarships;
