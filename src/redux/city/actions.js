import { FETCH_CITY } from "./constants";

export const fetchCitiesAsync = ({ type }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: "cities?_sort=name:ASC"
    }
  }
});

export const fetchCities = () => dispatch => {
  return dispatch(
    fetchCitiesAsync({
      type: FETCH_CITY
    })
  );
};
