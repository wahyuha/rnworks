import { FETCH_CITY } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  list: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const city = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CITY:
      return {
        ...state,
        isFetching: true
      };
    case success(FETCH_CITY):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        list: action.payload.data
      };
    case failure(FETCH_CITY):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default city;
