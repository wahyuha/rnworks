import { SEARCH_UNIVERSITY } from "./constants";
import { failure, success } from "../../utils/actionUtils";

const initialState = {
  results: {},
  isFetching: false,
  isLoaded: false,
  isFailed: false
};

const search = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_UNIVERSITY:
      return {
        ...state,
        isFetching: true
      };
    case success(SEARCH_UNIVERSITY):
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        isLoaded: true,
        results: action.payload.data
      };
    case failure(SEARCH_UNIVERSITY):
      return {
        ...state,
        isFetching: false,
        isFailed: true,
        message: action.msg
      };
    default:
      return state;
  }
};

export default search;
