import { SEARCH_UNIVERSITY } from "./constants";

export const searchUniversitiesAsync = ({ type, keyword }) => ({
  type,
  payload: {
    request: {
      method: "get",
      url: `universities?name_contains=${keyword}`
    }
  }
});

export const searchUniversities = keyword => dispatch => {
  return dispatch(
    searchUniversitiesAsync({
      type: SEARCH_UNIVERSITY,
      keyword
    })
  );
};
