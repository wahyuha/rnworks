import { AsyncStorage } from "react-native";
import { createStore, applyMiddleware, compose } from "redux";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import thunk from "redux-thunk";
import { persistReducer } from "redux-persist";
import { composeWithDevTools } from "remote-redux-devtools";
import { API_URL } from "react-native-dotenv";
import rootReducer from "../redux/combinedReducer";

const client = axios.create({
  baseURL: API_URL,
  responseType: "json",
  headers: {
    "X-Student": 123456
  }
});

const persistConfig = {
  key: "root",
  storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default function initializeStore() {
  const axiosMid = axiosMiddleware(client, {
    returnRejectedPromiseOnError: true
  });
  const middlewares = [thunk.withExtraArgument(), axiosMid];

  let debuggWrapper = data => data;
  debuggWrapper = composeWithDevTools({ realtime: true, name: "student" });

  const store = createStore(
    persistedReducer,
    {},
    debuggWrapper(compose(applyMiddleware(...middlewares)))
  );

  return store;
}
