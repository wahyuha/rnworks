// @flow

import React, { PureComponent } from "react";
import { Provider } from "react-redux";
import createStore from "./store";

class AppStoreProvider extends PureComponent {
  render() {
    const { children } = this.props;

    const store = createStore();

    return <Provider store={store}>{children}</Provider>;
  }
}

export default AppStoreProvider;
