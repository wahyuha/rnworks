/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable quotes */
import _ from "lodash";
import { Dimensions, View, Text } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import React from "react";
import Markdown from 'react-native-markdown-renderer';
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const scholarshipDetail = props => {
  const { detail } = props;
  if (_.isEmpty(detail)) return null;

  const { image } = detail;
  return (
    <View style={styles.detail_scho}>
      <View style={styles.cover}>
        <AutoHeightImage source={{ uri: image.url }} width={SCREEN_WIDTH} />
      </View>
      <View style={styles.content_scho}>
        <Text style={styles.titleH1}>{`SCHOLARSHIP • ${detail.title}`}</Text>
        <View className={styles.content}>
          <Markdown>{detail.description}</Markdown>
        </View>
      </View>
    </View>
  );
};

export default scholarshipDetail;
