import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  detail_scho: {
    backgroundColor: "white"
  },
  header: {},
  cover: {
    display: "flex",
    width: "100%",
    overflow: "hidden"
  },
  content_scho: {
    padding: 16
  },
  titleH1: {
    ...fontSize.LARGE,
    color: "#8491A3",
    ...fontWeight.BOLD,
    textTransform: "uppercase"
  },
  content: {
    paddingVertical: 8
  }
});

export default styles;
