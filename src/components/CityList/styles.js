import { StyleSheet, } from "react-native";
import { fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  city_wrap: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 12,
    borderBottomColor: "#E0E6ED",
    borderBottomWidth: 1
  },
  city_left: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  chip: {
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderRadius: 3,
    color: "#FFFFFF",
    ...fontWeight.BOLD
  },
  city_title: {
    paddingLeft: 8,
    ...fontWeight.MEDIUM
  },
  arrow_right: {
    width: 16,
    height: 16
  }
});

export default styles;
