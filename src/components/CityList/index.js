/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect } from "react";
import { FlatList, Image, Text, TouchableHighlight, View } from "react-native";
import { pushTo } from "@navigations";
import { LIST_UNIVERSITY } from "@navigations/screens";
import styles from "./styles";

function bindColor(index) {
  const COLORS = ["#D01F27", "#93C2A3", "#29A8A4", "#58A9C0", "#FF7465"];
  return index < 5 ? COLORS[index] : COLORS[index % 5];
}

function Item({ id, initial, name, componentId }, index) {
  const color = bindColor(index);
  const initName = initial || "AX0";
  const city = { id, name };
  return (
    <TouchableHighlight
      underlayColor="white"
      onPress={() => pushTo(componentId, LIST_UNIVERSITY, { props: { city } })}
    >
      <View style={styles.city_wrap}>
        <View style={styles.city_left}>
          <Text style={{ ...styles.chip, backgroundColor: color }}>
            {initName}
          </Text>
          <Text style={styles.city_title}>{name}</Text>
        </View>
        <Image
          style={styles.arrow_right}
          source={require("@icons/arrow-right-grey.png")}
        />
      </View>
    </TouchableHighlight>
  );
}

const cityList = props => {
  const { cities } = props;
  return (
    <FlatList
      data={cities}
      renderItem={({ item, index }) => Item({ ...item, ...props }, index)}
      keyExtractor={item => item.id}
    />
  );
};

export default cityList;
