/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import CardView from "react-native-cardview";
import { fontSize, fontWeight } from "@styles/fontSize";

const HeaderNav = () => {
  return (
    <CardView style={styles.header} cardElevation={8} cardMaxElevation={10}>
      <View style={styles.headNav}>
        <Image
          style={{ flex: 1, alignSelf: "stretch", width: null, height: null }}
          source={require("@images/icon_back.png")}
        />
      </View>
      <View style={styles.headTitle}>
        <Text style={{ ...fontSize.LARGE, ...fontWeight.BOLD }}>
          Universitas Unggulan
        </Text>
      </View>
    </CardView>
  );
};

const styles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000000"
  },
  headNav: {
    width: 56,
    height: 56,
    padding: 16
  },
  headTitle: {
    padding: 16,
    paddingLeft: 0,
    width: "100%"
  }
});

export default HeaderNav;
