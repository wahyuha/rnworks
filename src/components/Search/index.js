/* eslint-disable no-undef */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import _ from "lodash";
import React, { useState, useEffect } from "react";
import {
  View,
  ImageBackground,
  Text,
  TextInput,
  TouchableHighlight
} from "react-native";
import { dismissModalAll } from "@navigations";
import { ListUniversity } from "@components";
import { searchUniversities, getSearchResults } from "@redux/search";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";

const Search = props => {
  const dispatch = useDispatch();
  const [isSubmit, setSubmit] = useState(false);
  const [focusInit, setFocusInit] = useState(true);
  const [keyword, setkeyword] = useState(true);
  const handleSearch = keyword => {
    setkeyword(keyword);
    setSubmit(true);
    setFocusInit(false);
  };

  const SearchBar = props => {
    const [keyword, setKeyword] = useState("");
    return (
      <View style={styles.searchSection}>
        <TouchableHighlight
          style={styles.wrapClose}
          underlayColor="white"
          onPress={() => dismissModalAll()}
        >
          <ImageBackground
            source={require("@icons/close.png")}
            style={styles.iconClose}
          />
        </TouchableHighlight>
        <View style={styles.inputSection}>
          <ImageBackground
            source={require("@images/icon_search.png")}
            style={{ width: 14, height: 14 }}
          />
          <TextInput
            style={styles.searchInput}
            placeholder="Cari Universitas"
            autoFocus={focusInit}
            onChangeText={text => setKeyword(text)}
            onSubmitEditing={() => handleSearch(keyword)}
          />
        </View>
      </View>
    );
  };

  useEffect(() => {
    if (isSubmit) {
      dispatch(searchUniversities(keyword));
      setSubmit(false);
    }
  }, [isSubmit]);

  const showResults = () => {
    const { isFetching, isFailed, isLoaded, results } = useSelector(getSearchResults);
    const listProps = {
      ...props,
      isFailed,
      isFetching,
      isLoaded
    };
    if (isFetching || isFailed || isLoaded) {
      return (
        <View>
          <Text style={styles.resultHeader}>Hasil Pencarian</Text>
          <ListUniversity {...listProps} list={results} />
        </View>
      );
    }
  };

  return (
    <>
      <SearchBar {...props} />
      <View style={styles.searchBody}>{showResults()}</View>
    </>
  );
};

export default Search;
