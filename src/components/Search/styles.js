import { StyleSheet } from "react-native";
import { Dimensions } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const { height: SCREEN_HEIGHT } = Dimensions.get("window");

const appHeight = SCREEN_HEIGHT;

const styles = StyleSheet.create({
  searchSection: {
    display: "flex",
    flexDirection: "row",
    padding: 16,
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputSection: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 8,
    paddingRight: 12,
    paddingBottom: 8,
    paddingLeft: 12,
    borderRadius: 3,
    backgroundColor: "#E0E6ED"
  },
  searchInput: {
    paddingTop: 0,
    // paddingRight: 8,
    paddingBottom: 0,
    // paddingLeft: 8,
    borderWidth: 0,
    color: "#333333",
    backgroundColor: "#E0E6ED",
    width: "85%"
  },
  wrapClose: {
    padding: 8,
    paddingLeft: 0
  },
  iconClose: {
    width: 16,
    height: 16,
    marginRight: 8
  },
  resultHeader: {
    padding: 16,
    ...fontSize.LARGE,
    ...fontWeight.SEMIBOLD
  },
  searchBody: {
    height: appHeight,
    backgroundColor: "#E0E6ED"
  }
});

export default styles;
