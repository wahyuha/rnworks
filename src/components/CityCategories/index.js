/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import SkeletonLoader from "react-native-skeleton-loader";
import { Text, View } from "react-native";
import { CityList } from "@components"
import styles from "./styles";

const cityCategories = props => {
  const { isLoaded, isFetching, isFailed, cities } = props;
  const renderOrLoader = () => {
    if (isLoaded && cities.length > 0) {
      return <CityList {...props} />;
    } else if (isFailed) {
      return <Text>Error, please try again latter..</Text>;
    }
    return (
      <View style={{ marginTop: 16 }}>
        <SkeletonLoader
          type="rectangle"
          rows={10}
          height={42}
          loading
          color="#eaf0f7"
          hightlightColor="#edf3f9"
        />
      </View>
    );
  };
  return (
    <View style={styles.cc_wrapper}>
      <Text style={styles.cc_title}>Temukan Universitas di Kotamu</Text>
      {renderOrLoader()}
    </View>
  );
};

export default cityCategories;
