import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  cc_wrapper: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  cc_title: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
    paddingBottom: 16
  }
});

export default styles;
