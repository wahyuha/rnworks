/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import { backToPrevious } from "@navigations";
import styles from "./styles";

const coverComponent = props => {
  const { detail, componentId } = props;

  const { cover } = detail;
  const imageFile = cover ? cover.url : null;

  const TopBar = () => {
    return (
      <View style={styles.topBar}>
        <TouchableOpacity onPress={() => backToPrevious(componentId)}>
          <View style={styles.headNav}>
            <ImageBackground
              style={{
                flex: 1,
                alignSelf: "stretch",
                width: null,
                height: null
              }}
              source={require("@icons/back-white.png")}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.headTitle}>
          {/* <Text style={styles.headText} numberOfLines={1}>
            {detail.name}
          </Text> */}
        </View>
      </View>
    );
  };

  return (
    <>
      <TopBar />
      <View style={styles.cover}>
        {imageFile && (
          <Image style={styles.coverImg} source={{ uri: imageFile }} />
        )}
      </View>
    </>
  );
};

export default coverComponent;
