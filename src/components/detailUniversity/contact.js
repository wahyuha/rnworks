/* eslint-disable quotes */
import React from "react";
import _ from "lodash";
import { View, Image, Text } from "react-native";
import styles from "./styles";
import UniversityUtils from "@utils/university";

const contactComponent = props => {
  const { detail } = props;

  const labelInternational = () => {
    const { isInternational } = detail;
    if (!isInternational) return null;
    return (
      <View style={styles.international}>
        <Text>International</Text>
      </View>
    );
  };

  const accredited = () => {
    return (
      <View style={styles.accredited}>
        <Text style={styles.accreditedH4}>{`Accredited ${
          detail.accredited
        }`}</Text>
        {ratings()}
      </View>
    );
  };

  const ratings = () => {
    const ratingRounded = detail.average_rating
      ? Math.round(detail.average_rating)
      : 0;
    if (ratingRounded === 0) return null;

    return (
      <View style={styles.starsRating}>
        {[...Array(ratingRounded)].map((e, i) => (
          <Image source={require("@icons/star-review.png")} key={`st-${i}`} />
        ))}
        <View style={styles.starsRatingSpan}>
          <Text>{detail.average_rating}</Text>
        </View>
      </View>
    );
  };

  const contact = () => {
    const { city } = detail;
    // const { province } = city;
    // const completeAddress = `${detail.address}, ${detail.sub_district}, ${city.name}, ${province.name || ""} `;
    const completeAddress = `${detail.address}, ${detail.sub_district}, ${
      city ? city.name : ""
    } `;
    const completePhone = `${detail.phone}, ${detail.phone_2}`;
    const expenseObj = UniversityUtils.getExpenseStatus(detail);
    return (
      <View style={styles.contactDetail}>
        <View style={styles.contactRow}>
          <View style={styles.contactIcon}>
            <Image source={require("@icons/address.png")} />
          </View>
          <Text style={styles.ctc_text}>{completeAddress || "-"}</Text>
        </View>
        <View style={styles.contactRow}>
          <View style={styles.contactIcon}>
            <Image source={require("@icons/phone.png")} />
          </View>
          <Text style={styles.ctc_text}>{completePhone || "-"}</Text>
        </View>
        <View style={styles.contactRow}>
          <View style={styles.contactIcon}>
            <Image source={require("@icons/address.png")} />
          </View>
          <Text style={styles.ctc_text}>{detail.website}</Text>
        </View>
        <View style={styles.contactRow}>
          <View style={styles.contactIcon}>
            <Image source={require("@icons/expense.png")} />
          </View>
          <Text style={styles.ctc_text}>{expenseObj.label}</Text>
        </View>
        {announcement()}
        {/* {otherCampuses()} */}
      </View>
    );
  };

  const announcement = () => {
    if (!detail.announcement) return null;
    return (
      <View style={styles.announcement}>
        <View style={styles.announceImg}>
          <Image source={require("@icons/announcement.png")} />
        </View>
        <Text>{detail.announcement}</Text>
      </View>
    );
  };

  const otherCampuses = () => {
    return (
      <View style={styles.others}>
        <>
          <Text style={styles.othersA}>View other campuses’ locations (4)</Text>
          <Image source={require("@icons/arrow-right.png")} />
        </>
      </View>
    );
  };

  return (
    <View>
      {labelInternational()}
      <Text style={styles.contentInfoH2}>{detail.name}</Text>
      {accredited()}
      {contact()}
    </View>
  );
};

export default contactComponent;
