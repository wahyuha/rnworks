/* eslint-disable quotes */
import React from "react";
import { View, StatusBar } from "react-native";
import ContactComponent from "./contact";
import CoverComponent from "./cover";
import FacilitiesComponent from "./facilities";
import FacultiesComponent from "./faculties";
import GalleriesComponent from "./galeries";
import NewsComponent from "./news";
import ScholarshipsComponent from "./scholarships";
import styles from "./styles";

const detailUniversity = props => {
  return (
    <View style={styles.detail_wrapper}>
      <StatusBar backgroundColor="#467EAB" barStyle="light-content" />
      <CoverComponent {...props} />
      <View style={styles.contentInfo}>
        <ContactComponent {...props} />
        <View style={styles.borders} />
        <FacultiesComponent {...props} />
        <FacilitiesComponent {...props} />
        <ScholarshipsComponent {...props} />
        <NewsComponent {...props} />
        <GalleriesComponent {...props} />
      </View>
    </View>
  );
};

export default detailUniversity;
