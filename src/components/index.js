import ButtonTabs from "./ButtonTabs";
import CityCategories from "./CityCategories";
import CityList from "./CityList";
import DetailUniversity from "./DetailUniversity";
import Home from "./Home";
import ListUniversity from "./ListUniversity";
import NewsDetail from "./NewsDetail";
import ScholarshipDetail from "./ScholarshipDetail";
import Search from "./Search";
import SearchBar from "./SearchBar";

export {
  ButtonTabs,
  CityCategories,
  CityList,
  DetailUniversity,
  Home,
  ListUniversity,
  NewsDetail,
  ScholarshipDetail,
  Search,
  SearchBar
};
