import React, { useEffect } from "react";
import AutoHeightImage from "react-native-auto-height-image";
import SkeletonLoader from "react-native-skeleton-loader";
// eslint-disable-next-line prettier/prettier
import { Dimensions, ScrollView, Text, TouchableHighlight, View } from "react-native";
import { pushTo } from "@navigations";
import { DETAIL_SCREEN, LIST_UNIVERSITY } from "@navigations/screens";
import {
  fetchFeaturedUniversities,
  getFeaturedUniversities
} from "@redux/featuredUniversities";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");
const FeaturedUniversities = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchFeaturedUniversities());
  }, []);

  const { isFetching, isLoaded, featured } = useSelector(
    getFeaturedUniversities
  );

  const renderOrLoading = () => {
    if (isLoaded && featured.length > 0) {
      return (
        <ScrollView style={styles.featured_slider} horizontal>
          {featured.map((univ, index) =>
            featuredUnivItem(props.componentId, univ, index)
          )}
        </ScrollView>
      );
    }
    return <SkeletonLoader type="rectangle" height={150} loading />;
  };

  return (
    <View style={styles.featured_univ}>
      <View style={styles.univ_header}>
        <Text style={styles.univ_h3}>Universitas Unggulan</Text>
        <TouchableHighlight
          underlayColor="white"
          onPress={() => pushTo(props.componentId, LIST_UNIVERSITY)}
        >
          <Text style={styles.see_all}>Lihat Semua</Text>
        </TouchableHighlight>
      </View>
      <View style={styles.featured_slider_wrap}>{renderOrLoading()}</View>
    </View>
  );
};

const featuredUnivItem = (componentId, university, index) => {
  return (
    <TouchableHighlight
      underlayColor="white"
      onPress={() =>
        pushTo(componentId, DETAIL_SCREEN, {
          props: { detail: university, append: true }
        })
      }
      key={`unv-${index}`}
    >
      <View style={styles.slider_item}>
        <View style={styles.featured_wrap}>
          <AutoHeightImage
            source={{ uri: university.cover.url }}
            width={SCREEN_WIDTH / 1.8}
          />
        </View>
        <Text style={styles.featured_h2}>{university.name}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default FeaturedUniversities;
