import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  container_cities: {
    padding: 16,
    paddingTop: 24
  },
  featured_univ: {
    paddingTop: 8,
    paddingHorizontal: 16
  },
  univ_header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    ...fontWeight.BOLD
  },
  univ_h3: {
    paddingBottom: 8,
    ...fontSize.LARGE,
    ...fontWeight.BOLD
  },
  see_all: {
    color: "#60B2A4",
    paddingBottom: 8,
    ...fontSize.MEDIUM,
    ...fontWeight.MEDIUM
  },
  featured_slider_wrap: {
    width: "100%"
  },
  loader_wap: {
    display: "flex",
    flexDirection: "row"
  },
  featured_slider: {
    display: "flex",
    flexDirection: "row",
  },
  slider_item: {
    paddingRight: 16
  },
  featured_h2: {
    position: "absolute",
    bottom: 0,
    color: "#FFFFFF",
    padding: 8,
    marginRight: 16,
    backgroundColor: "#5647474d"
  },
  featured_wrap: {
    display: "flex",
    justifyContent: "center",
    overflow: "hidden",
    borderRadius: 3
  },
  featured_wrap_img: {
    height: "110px"
  },
  home_content: {
    padding: 16
  },
  borders: {
    width: "100%",
    marginVertical: 8,
    height: 1,
    backgroundColor: "#E0E6ED"
  },
  scholar: {
    paddingVertical: 8,
    ...fontWeight.BOLD
  },
  sh_h3: {
    paddingBottom: 8,
    ...fontSize.LARGE,
    ...fontWeight.BOLD
  },
  scholar_header: {
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center"
  },
  scholar_row: {
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "row",
    paddingVertical: 6
  },
  scholar_img: {
    paddingRight: 8
  },
  sch_title: {
    ...fontWeight.REGULAR
  }
});

export default styles;

// .home_wrapper {}

// .header_board_1 {
//   background: url('./assets/home_bg.jpg') no-repeat center;
//   background-size: 100%;
// }

// .header_board_2 {
//   background: url('./assets/home_bg1.jpg') no-repeat center;
//   background-size: 100%;
// }

// .board_content {
//   padding: 16px;
//   background-color: rgba(0, 0, 0, 0.3);
//   min-height: 100px;
//   color: var(--color-white);

//   .label_h {
//     text-align: center;

//     .label_left { color: var(--color-red) }
//     .label_right { color: var(--color-white) }
//   }

//   p {
//     padding: 10px 0;
//     text-align: center;
//   }
// }

// .search_section{
//   display: flex;
//   flex-direction: column;
//   align-items: center;

//   .search_h {
//     display: flex;
//     align-items: center;
//     width: 80%;
//     padding: 8px 12px;
//     border-radius: 3px;
//     background-color: var(--color-white);

//     i {
//       width: 14px;
//       height: 14px;
//       background-image: url('./assets/icon-search.svg');
//       background-position: center;
//       background-repeat: no-repeat;
//     }

//     input {
//       padding: 0 8px;
//       border: none;
//       color: var(--color-black);
//       line-height: 2rem;

//       &::placeholder {
//         color: var(--color-white-smoke);
//       }
//     }
//   }
//   button {
//     margin-top: 12px;
//     padding: 8px 0;
//     width: 80%;
//     background-color: var(--color-red);
//     color: var(--color-white);
//     border-radius: 3px;
//   }
// }

// .home_content {
//   padding: 16px;
// }

// .borders {
//   width: 100%;
//   margin: 8px 0;
//   height: 1px;
//   background-color: var(--color-gray-light);
// }

// .scholar {
//   padding: 8px 0;
//   font-weight: 600;

//   h3 {
//     padding-bottom: 8px;
//     font-size: 1.6rem;
//     font-weight: 600;
//   }
// }

// .scholar_header {
//   display: flex;
//   justify-content: space-between;

//   a {
//     color: var(--color-green-light);
//   }
// }

// .enroll {
//   padding: 8px 0;
//   font-weight: 600;

//   h3 {
//     padding-bottom: 8px;
//     font-size: 1.6rem;
//     font-weight: 600;
//   }

//   .enroll_list {
//     display: flex;
//     align-items: center;
//     padding: 8px 0;

//     .enroll_item {
//       display: flex;
//       justify-content: center;
//       width: 25%;
//       margin-right: 8px;
//       overflow: hidden;
//       border-radius: 3px;

//       &:last-child {
//         margin-right: 0;
//       }
//     }
//     img {
//       height: 90px;
//     }
//   }

//   .uni_info {
//     display: flex;
//     justify-content: space-between;
//     align-items: center;

//     h2 {
//       font-weight: 600;
//     }

//     h5 {
//       font-size: 1.2rem;
//       color: var(--color-white-smoke);
//     }
//   }

//   .uni_city_info { color: var(--color-white-smoke) }
// }

// .enroll_header {
//   display: flex;
//   justify-content: space-between;

//   a {
//     color: var(--color-green-light);
//   }
// }

// .announcement {
//   display: flex;
//   align-items: center;
//   margin: 8px 0;
//   padding: 8px 16px;
//   background-color: var(--color-orange-light);
//   border-radius: 3px;

//   font-size: 1.2rem;
//   line-height: 1.4rem;

//   .announce_img {
//     padding-right: 8px;
//     img {
//       width: 22px;
//     }
//   }
// }
