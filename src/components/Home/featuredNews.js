/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect } from "react";
import AutoHeightImage from "react-native-auto-height-image";
import SkeletonLoader from "react-native-skeleton-loader";
import {
  Dimensions,
  ActivityIndicator,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { pushTo } from "@navigations";
import { SCHOLARSHIPS } from "@navigations/screens";
import { fetchFeaturedNews, getFeaturedNews } from "@redux/featuredNews";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const featuredNews = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchFeaturedNews());
  }, []);

  const { isFetching, isLoaded, featured } = useSelector(getFeaturedNews);
  const renderOrLoader = () => {
    if (isFetching) {
      return (
        <>
          {[...Array(2)].map((v, i) => (
            <View style={styles.scholar_row}>
              <View style={{ width: "35%" }}>
                <SkeletonLoader type="rectangle" height={70} loading />
              </View>
              <View style={{ width: "60%", paddingLeft: 8 }}>
                <SkeletonLoader type="rectangle" height={16} loading />
                <SkeletonLoader type="rectangle" height={10} loading />
              </View>
            </View>
          ))}
        </>
      );
    } else if (isLoaded && featured.length > 0) {
      return featured.map((scho, index) => NewsItem(props, scho, index));
    }
  };

  return (
    <View style={styles.scholar}>
      <View style={styles.scholar_header}>
        <Text style={styles.sh_h3}>Campus Updates</Text>
        <TouchableHighlight
          underlayColor="white"
          onPress={() => pushTo(props.componentId, null)}
        >
          <Text style={styles.see_all}>Lihat Semua</Text>
        </TouchableHighlight>
      </View>
    {renderOrLoader()}
    </View>
  );
};

const NewsItem = (props, news, index) => {
  const { title, image } = news;
  return (
    <TouchableHighlight
      underlayColor="white"
      onPress={() =>
        pushTo(props.componentId, SCHOLARSHIPS, {
          props: { detail: news },
          topBar: { visible: true, title }
        })
      }
      key={`nws-${index}`}
    >
      <View style={styles.scholar_row}>
        <View  style={styles.scholar_img}>
          {image && (
            <AutoHeightImage
              source={{ uri: image.url }}
              width={SCREEN_WIDTH / 3}
            />
          )}
        </View>
        <Text style={styles.sch_title}>{title}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default featuredNews;
