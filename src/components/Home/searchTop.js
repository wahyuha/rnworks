/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { View, ImageBackground, StyleSheet, TextInput } from "react-native";
import { showModal } from "@navigations";
import { SEARCH } from "@navigations/screens";

const SearchBar = () => {
  return (
    <View style={styles.searchSection}>
      <ImageBackground
        source={require("@icons/logo_student.png")}
        style={styles.iconFilter}
      />
      <View style={styles.inputSection}>
        <ImageBackground
          source={require("@images/icon_search.png")}
          style={{ width: 14, height: 14 }}
        />
        <TextInput
          style={styles.searchInput}
          placeholder="Cari Universitas"
          onFocus={() => showModal(SEARCH)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    display: "flex",
    flexDirection: "row",
    padding: 16,
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputSection: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 8,
    paddingRight: 12,
    paddingBottom: 8,
    paddingLeft: 12,
    borderRadius: 3,
    backgroundColor: "#E0E6ED"
  },
  searchInput: {
    paddingTop: 0,
    paddingBottom: 0,
    borderWidth: 0,
    color: "#333333",
    backgroundColor: "#E0E6ED",
    width: "75%"
  },
  iconFilter: {
    width: 32,
    height: 24,
    padding: 4,
    marginRight: 8
  }
});

export default SearchBar;
