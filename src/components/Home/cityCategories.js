/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { Text, TouchableHighlight, View } from "react-native";
import { CityList } from "@components"
import { pushTo } from "@navigations";
import { CITY_CATEGORIES } from "@navigations/screens";
import styles from "./styles";

const DATA = [
  {
    name: "Jakarta",
    initial: "JKT",
    id: 1
  },
  {
    name: "Yogyakarta",
    initial: "YOG",
    id: 12
  },
  {
    name: "Bandung",
    initial: "BDG",
    id: 11
  },
  {
    name: "Tangerang",
    initial: "TNG",
    id: 3
  },
  {
    name: "Malang",
    initial: "MLG",
    id: 13
  }
];

const cityCategories = props => {
  const citiesProps = { ...props, cities: DATA };
  return (
    <View style={styles.container_cities}>
      <View style={styles.scholar_header}>
        <Text style={styles.sh_h3}>Universitas di Kotamu</Text>
        <TouchableHighlight
          underlayColor="white"
          onPress={() => pushTo(props.componentId, CITY_CATEGORIES)}
        >
          <Text style={styles.see_all}>Lihat Semua</Text>
        </TouchableHighlight>
      </View>
      <CityList {...citiesProps} />
    </View>
  );
};

export default cityCategories;
