import React, { useEffect } from "react";
import AutoHeightImage from "react-native-auto-height-image";
import SkeletonLoader from "react-native-skeleton-loader";
import {
  Dimensions,
  Text,
  TouchableHighlight,
  View,
  Button
} from "react-native";
import { pushTo } from "@navigations";
import { SCHOLARSHIPS } from "@navigations/screens";
import {
  fetchFeaturedScholarships,
  getFeaturedScholarships
} from "@redux/featuredScholarships";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const FeaturedScholarships = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchFeaturedScholarships());
  }, []);

  const { isFetching, isLoaded, featured } = useSelector(
    getFeaturedScholarships
  );
  const renderOrLoader = () => {
    if (isFetching) {
      return (
        <>
          {[...Array(2)].map((v, i) => (
            <View style={styles.scholar_row}>
              <View style={{ width: "35%" }}>
                <SkeletonLoader type="rectangle" height={70} loading />
              </View>
              <View style={{ width: "60%", paddingLeft: 8 }}>
                <SkeletonLoader type="rectangle" height={16} loading />
                <SkeletonLoader type="rectangle" height={10} loading />
              </View>
            </View>
          ))}
        </>
      );
    } else if (isLoaded && featured.length > 0) {
      return featured.map((scho, index) =>
        ScholarshipsItem(props, scho, index)
      );
    }
  };

  return (
    <View style={styles.scholar}>
      <View style={styles.scholar_header}>
        <Text style={styles.sh_h3}>Scholarship Program</Text>
        <TouchableHighlight
          underlayColor="white"
          onPress={() => pushTo(props.componentId, null)}
        >
          <Text style={styles.see_all}>Lihat Semua</Text>
        </TouchableHighlight>
      </View>
      {renderOrLoader()}
    </View>
  );
};

const ScholarshipsItem = (props, scholarship, index) => {
  const { title, image } = scholarship;
  return (
    <TouchableHighlight
      underlayColor="white"
      onPress={() =>
        pushTo(props.componentId, SCHOLARSHIPS, {
          props: { detail: scholarship },
          topBar: { visible: true, title }
        })
      }
      key={`sch-${index}`}
    >
      <View style={styles.scholar_row}>
        <View style={styles.scholar_img}>
          {image && (
            <AutoHeightImage
              source={{ uri: image.url }}
              width={SCREEN_WIDTH / 3}
            />
          )}
        </View>
        <Text style={styles.sch_title}>{title}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default FeaturedScholarships;
