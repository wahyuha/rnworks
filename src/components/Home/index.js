import React from "react";
import { View } from "react-native";
import SearchTop from "./searchTop";
import CityCategories from "./cityCategories";
import FeaturedNews from "./featuredNews";
import FeaturedUniversities from "./featuredUniversities";
import FeaturedScholarships from "./featuredScholarships";
import styles from "./styles";

const home = props => {
  return (
    <View>
      <SearchTop />
      <FeaturedUniversities {...props} />
      <CityCategories {...props} />
      <View style={styles.home_content}>
        <FeaturedScholarships {...props} />
        {/* <View style={styles.borders} /> */}
        {/* {this.enrollment()} */}
        <View style={styles.borders} />
        <FeaturedNews {...props} />
      </View>
    </View>
  );
};

export default home;
