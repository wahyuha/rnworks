import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  listWrapper: {
    paddingVertical: 0,
    paddingHorizontal: 16,
    backgroundColor: "#FFFFFF"
  },
  itemUniversity: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    marginTop: 8,
    marginBottom: 24
  },
  uniInfo: {
    flex: 1,
    flexGrow: 1,
    paddingRight: 16
  },
  uniName: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD
  },
  uniIsIntl: {
    ...fontSize.SMALL,
    ...fontWeight.MEDIUM,
    color: "#467EAB"
  },
  listCity: {
    color: "#8491A3",
    paddingTop: 6,
    lineHeight: fontSize.MEDIUM.fontSize,
    ...fontWeight.SEMIBOLD
  },
  listAccr: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 6
  },
  seeAll: {
    display: "flex",
    flexDirection: "row",
    paddingTop: 4
  },
  listFacult: {
    lineHeight: fontSize.LARGE.fontSize,
    ...fontWeight.REGULAR
  },
  uniImage: {
    overflow: "hidden"
  },
  expense: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row"
  }
});

export default styles;
