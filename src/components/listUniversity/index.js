/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React, { Fragment } from "react";
import _ from "lodash";
import AutoHeightImage from "react-native-auto-height-image";
import SkeletonLoader from "react-native-skeleton-loader";
// eslint-disable-next-line prettier/prettier
import { Dimensions, View, Image, Text, TouchableHighlight } from "react-native";
import { pushTo } from "@navigations";
import { DETAIL_SCREEN } from "@navigations/screens";
import { fontWeight } from "@styles/fontSize";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const ListUniversity = props => {
  const { componentId, list } = props;

  const itemUniversity = (university, i) => {
    const detail = _.find(list, { id: university.id });
    return (
      <TouchableHighlight
        underlayColor="white"
        onPress={() =>
          pushTo(componentId, DETAIL_SCREEN, { props: { detail } })
        }
        key={`uni-${i}`}
      >
        <View style={styles.itemUniversity}>
          <View style={styles.uniInfo}>
            {isInternational(university)}
            <Text style={styles.uniName}>{university.name}</Text>
            {city(university)}
            {level(university)}
            <Text style={styles.listFacult}>
              Computing, Computer Sience, Information Technology, Accounting and
              more
            </Text>
            <View style={styles.seeAll}>
              <Text style={{ ...fontWeight.SEMIBOLD }}>Lihat Semua: </Text>
              <Text style={{ color: "#60B2A4", ...fontWeight.SEMIBOLD }}>
                Faculties and Programs (19)
              </Text>
            </View>
          </View>
          {cover(university)}
        </View>
      </TouchableHighlight>
    );
  };

  const isInternational = university => {
    if (university.isInternational) {
      return <Text style={styles.uniIsIntl}>INTERNATIONAL</Text>;
    }
    return null;
  };

  const city = university => {
    const { city } = university;
    if (!_.isEmpty(city)) return <Text style={styles.listCity}>{city.name}</Text>;
    else return null;
  };

  const level = university => {
    const { expense, accredited } = university;
    let numOfIcon = 0;
    if (expense === "LOW") numOfIcon = 1;
    else if (expense === "MID") numOfIcon = 2;
    else if (expense === "HIGH") numOfIcon = 3;
    return (
      <View style={styles.listAccr}>
        <Text>{accredited ? `Akreditasi ${accredited} •` : ""} </Text>
        <View style={styles.expense}>
          {[...Array(numOfIcon)].map((v, i) => (
            <Fragment key={`exp-${i}`}>
              <Text> </Text>
              <Image
                style={{ width: 8, height: 16, paddingHorizontal: 2 }}
                source={require("@images/icon_expense.png")}
              />
              <Text> </Text>
            </Fragment>
          ))}
        </View>
      </View>
    );
  };

  const cover = university => {
    const { cover } = university;
    let sourceProps = { source: require("@images/university_default.png") }; // TODO: provide image placeholder
    if (!_.isEmpty(cover)) sourceProps = { source: { uri: cover.url } };
    return (
      <View style={styles.uniImage}>
        <AutoHeightImage
          {...sourceProps}
          width={SCREEN_WIDTH / 3}
          style={{ borderRadius: 4 }}
        />
        {/* <Image
          {...sourceProps}
          style={{ width: 96, height: 72, borderRadius: 4 }}
        /> */}
      </View>
    );
  };

  const { isFetching, isLoaded, isFailed } = props;

  if (isLoaded) {
    if (_.isEmpty(list))
      return (
        <Text style={{ padding: 16, backgroundColor: "#FFFFFF" }}>
          Data tidak ditemukan
        </Text>
      );
    return (
      <View style={styles.listWrapper}>
        {list.map((university, i) => itemUniversity(university, i))}
      </View>
    );
  } else if (isFailed) {
    return <Text>Error.. Please try again latter</Text>;
  }
  return (
    <View style={styles.listWrapper}>
      {[...Array(6)].map((v, i) => (
        <View style={styles.itemUniversity}>
          <View style={styles.uniInfo}>
            <View style={{ ...styles.uniName, paddingBottom: 8 }}>
              <SkeletonLoader type="rectangle" height={18} loading />
            </View>
            <SkeletonLoader type="rectangle" rows={2} height={12} loading />
          </View>
          <View style={styles.uniImage}>
            <View style={{ width: 120, height: 72, borderRadius: 4 }}>
              <SkeletonLoader type="rectangle" height={100} loading />
            </View>
          </View>
        </View>
      ))}
    </View>
  );
};

export default ListUniversity;
