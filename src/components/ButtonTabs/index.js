import React from "react";
import { View, Button, Text, TouchableOpacity } from "react-native";
import styles from "./styles.js";

class ButtonTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      options: props.options
    };
  }

  selectThis(value) {
    this.setState(prevState => {
      return prevState.options.map(option => {
        delete option.selected;
        const newOptions = option;
        if (option.value === value) newOptions.selected = true;

        return newOptions;
      });
    });
    this.props.onSelect && this.props.onSelect(value);
  }

  buttonTabs({ value, label, selected }) {
    const isActive = selected ? styles.facTabsActive : {};
    const btClass = {
      ...styles.facTabsButton,
      ...isActive
    };
    return (
      <TouchableOpacity
        onPress={() => this.selectThis.bind(this)(value)}
        style={btClass}
      >
        <Text
          style={({ ...styles.ctc_text }, selected ? styles.activeText : {})}
        >
          {label}
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.facTabs}>
        {this.state.options.map(option => this.buttonTabs(option))}
      </View>
    );
  }
}

export default ButtonTabs;
