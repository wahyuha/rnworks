import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  facTabs: {
    paddingVertical: 8,
    paddingHorizontal: 0,
    display: "flex",
    flexDirection: "row"
  },
  facTabsButton: {
    marginRight: 8,
    paddingVertical: 4,
    paddingHorizontal: 12,
    borderRadius: 12,
    backgroundColor: "#FFFFFF",
    color: "#333333",
    borderStyle: "solid",
    borderColor: "#E0E6ED",
    borderWidth: 1
  },
  facTabsActive: {
    backgroundColor: "#60B2A4"
  },
  activeText: {
    color: "#FFFFFF",
    ...fontWeight.SEMIBOLD
  },
  ctc_text: {
    ...fontWeight.MEDIUM
  }
});

export default styles;
