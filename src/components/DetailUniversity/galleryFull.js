import React from "react";
import { Image, ImageBackground, TouchableOpacity } from "react-native";
import { dismissModalAll } from "@navigations";
import _ from "lodash";
import Gallery from "react-native-image-gallery";
import styles from "./styles";

const GalleryFull = props => {
  const { detail, position } = props;
  const { cover, images } = detail;

  const imageGallery = [];
  cover &&
    imageGallery.push({
      source: { uri: cover.url }
    });
  images.map(image => {
    imageGallery.push({
      source: { uri: image.image.url }
    });
  });

  return (
    <>
      <ImageBackground style={styles.fullMode}>
        <TouchableOpacity onPress={() => dismissModalAll()}>
          <Image
            source={require("@icons/back-white.png")}
            style={styles.backIcon}
          />
        </TouchableOpacity>
        <Gallery
          initialPage={position || 0}
          images={imageGallery}
          onLongPress={() => dismissModalAll()}
        />
      </ImageBackground>
    </>
  );
};

export default GalleryFull;
