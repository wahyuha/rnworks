import React from "react";
import _ from "lodash";
import { View, Dimensions, Text, TouchableOpacity } from "react-native";
import AutoHeightImage from "react-native-auto-height-image";
import { showModal } from "@navigations";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const Galleries = props => {
  const { detail } = props;
  const { cover } = detail;
  let { images } = detail;

  if (_.isEmpty(images)) {
    return null;
  }

  const overLength = images.length > 5;
  if (overLength) images = images.slice(0, 4);

  return (
    <>
      <View style={styles.album}>
        <Text style={styles.albumH3}>Photo Album</Text>
        <View style={styles.galleries}>
          <TouchableOpacity
            style={styles.thumbnail}
            onPress={() => showModal("gallery.full", { props: { detail } })}
          >
            <AutoHeightImage
              source={{ uri: cover.url }}
              style={styles.thumbnailImg}
              resizeMode={"contain"}
              width={SCREEN_WIDTH / 2}
            />
          </TouchableOpacity>
          <View style={styles.gallery}>
            {images.map((gallery, index) => {
              const { image } = gallery;
              return (
                <TouchableOpacity
                  style={styles.galleryItem}
                  key={`imgs-${image.id}`}
                  onPress={() =>
                    showModal("gallery.full", {
                      props: { detail, position: index + 1 }
                    })
                  }
                >
                  <AutoHeightImage
                    source={{ uri: image.url }}
                    style={styles.galleryItemImg}
                    width={SCREEN_WIDTH / 4}
                  />
                  {overLength && index === images.length - 1 ? (
                    <Text style={styles.galleryMore}>More +</Text>
                  ) : null}
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      </View>
    </>
  );
};

export default Galleries;
