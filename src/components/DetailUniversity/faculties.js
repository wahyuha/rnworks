/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from "react";
import _ from "lodash";
import { View, Image, Text, TouchableOpacity } from "react-native";
import { backToPrevious } from "@navigations";
import { ButtonTabs } from "@components";
import UniversityUtils from "@utils/university";
import styles from "./styles";

const facilities = props => {
  const { detail, componentId } = props;
  const { faculty_degree: faculties } = detail;
  const grouped = UniversityUtils.facultiesByDegree(faculties);
  const [selectedGroup, setSelectedGroup] = useState("ALL");

  if (_.isEmpty(grouped['ALL'])) return null;

  const tabs = ({ grouped, selectedGroup, setSelectedGroup }) => {
    const options = [];
    Object.keys(grouped).map(key =>
      options.push({
        label: key,
        value: key,
        selected: key === selectedGroup
      })
    );
    return (
      <ButtonTabs
        options={options}
        onSelect={selected => setSelectedGroup(selected)}
      />
    );
  };

  const facultyCategories = faculties => {
    const facultiesGroup = UniversityUtils.facultiesByParent(faculties);
    return (
      <View style={styles.facCategories}>
        {Object.keys(facultiesGroup).map(group =>
          groupFaculties(group, facultiesGroup[group])
        )}
      </View>
    );
  };

  const groupFaculties = (group, facultiesGroup) => {
    return (
      <React.Fragment>
        {/* <Text style={styles.facCategoriesH4}>{group}</Text> */}
        <View style={styles.facCategoriesUl}>
          {facultiesGroup.map(child => (
            <View style={styles.facilitiesList} key={`fac-${child.faculty.id}`}>
              <Text style={styles.ctc_text}>{child.faculty.name}</Text>
            </View>
          ))}
        </View>
      </React.Fragment>
    );
  };

  // const otherFaculties = () => {
  //   return (
  //     <View style={styles.others}>
  //       <Link href="">
  //         <React.Fragment>
  //           <a>View all faculties & programs (14)</a>
  //           <img
  //             src="/static/icons/icon-arrow-right.svg"
  //             alt="other faculties"
  //           />
  //         </React.Fragment>
  //       </Link>
  //     </View>
  //   );
  // };

  return (
    <>
      <View style={styles.faculties}>
        <Text style={styles.facultiesH3}>Faculties & Programs</Text>
        {tabs({ grouped, selectedGroup, setSelectedGroup })}
        {facultyCategories(grouped[selectedGroup])}
        {/* {this.otherFaculties()} */}
      </View>
      <View style={styles.borders} />
    </>
  );
};

export default facilities;
