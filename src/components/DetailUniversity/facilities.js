import React from "react";
import { View, Text, FlatList } from "react-native";
import styles from "./styles";

const Facilities = props => {
  const { detail } = props;
  const { facilities } = detail;
  if (!facilities) return null;
  return (
    <>
      <View style={styles.facilities}>
        <Text style={styles.facilitiesH3}>Facilities</Text>
        <FlatList
          data={facilities}
          renderItem={({ item }) => (
            <Text style={styles.facilitiesList}>• {item.name}</Text>
          )}
        />
      </View>
      <View style={styles.borders} />
    </>
  );
};

export default Facilities;
