import _ from "lodash";
import { Dimensions, View, Text, TouchableOpacity } from "react-native";
import { pushTo } from "@navigations";
import { SCHOLARSHIPS } from "@navigations/screens";
import AutoHeightImage from "react-native-auto-height-image";
import React from "react";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const Scholarships = props => {
  const { detail, componentId } = props;
  const { scholarships } = detail;

  const ScholarshipsItem = detail => {
    const { image } = detail;
    return (
      <TouchableOpacity
        onPress={() =>
          pushTo(componentId, SCHOLARSHIPS, {
            props: { detail },
            topBar: { visible: true, title: detail.title }
          })
        }
      >
        <View style={styles.scholar_row}>
          <View style={styles.scholar_img}>
            {image && (
              <AutoHeightImage
                source={{ uri: image.url }}
                // style={styles.galleryItemImg}
                width={SCREEN_WIDTH / 3}
              />
            )}
          </View>
          <Text style={styles.ctc_text}>{detail.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  if (_.isEmpty(scholarships)) return null;
  return (
    <>
      <View style={styles.scholar}>
        <Text style={styles.scholarH3}>Scholarship Program</Text>
        {scholarships.map(scholarship => (
          <ScholarshipsItem {...scholarship} />
        ))}
      </View>
      <View style={styles.borders} />
    </>
  );
};

export default Scholarships;
