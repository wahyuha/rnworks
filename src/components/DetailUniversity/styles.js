import { StyleSheet } from "react-native";
import { fontSize, fontWeight } from "@styles/fontSize";

const styles = StyleSheet.create({
  detail_wrapper: {
    position: "relative",
    backgroundColor: "#FFFFFF"
  },
  listWrapper: {
    paddingVertical: 0,
    paddingHorizontal: 16
  },
  topBar: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
    top: 0,
    zIndex: 5
  },
  headNav: {
    width: 56,
    height: 56,
    padding: 16
  },
  headTitle: {
    padding: 16,
    paddingLeft: 0,
    width: "100%",
    flex: 1
  },
  headText: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
    zIndex: 5
  },
  cover: {
    flexDirection: "row",
    alignItems: "center",
    maxHeight: 224,
    position: "relative",
    marginBottom: 8,
    width: "100%",
    overflow: "hidden"
  },
  coverImg: {
    width: "100%",
    height: 224,
    zIndex: 1,
    position: "relative"
  },
  header: {
    position: "absolute",
    top: "0",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  headerIcons: {
    paddingVertical: 12,
    paddingHorizontal: 16
  },
  headerIconsImg: {
    width: 20
  },
  contentInfo: {
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  contentInfoH2: {
    ...fontSize.XLARGE,
    ...fontWeight.BOLD,
    color: "#333333",
    lineHeight: fontSize.XLARGE.fontSize,
    paddingVertical: 6,
    paddingHorizontal: 0
  },
  international: {
    ...fontSize.SMALL,
    color: "#467EAB",
    ...fontWeight.BOLD,
    textTransform: "uppercase"
  },
  accredited: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 6,
    paddingHorizontal: 0
  },
  accreditedH4: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
    color: "#333333"
  },
  starsRating: {
    flexDirection: "row",
    alignItems: "center"
  },
  starsRatingImg: {
    height: 12
  },
  starsRatingSpan: {
    ...fontWeight.BOLD,
    ...fontSize.MEDIUM,
    paddingLeft: 4,
    lineHeight: fontSize.MEDIUM.fontSize
  },
  contactDetail: {
    paddingVertical: 6,
    paddingHorizontal: 1
  },
  contactRow: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingBottom: 4
  },
  contactIcon: {
    width: 24,
    paddingTop: 2
  },
  ctc_text: {
    ...fontWeight.REGULAR
  },
  announcement: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 8,
    marginHorizontal: 0,
    paddingVertical: 8,
    paddingHorizontal: 16,
    backgroundColor: "#FBE5C4",
    borderRadius: 3,
    ...fontSize.MEDIUM,
    lineHeight: fontSize.LARGE.fontSize
  },
  announceImg: {
    paddingRight: 8
  },
  announceImgImg: {
    width: 22
  },
  others: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 8,
    paddingHorizontal: 0
  },
  othersA: {
    ...fontSize.LARGE,
    color: "#60B2A4",
    ...fontWeight.BOLD,
  },
  borders: {
    width: "100%",
    marginVertical: 8,
    marginHorizontal: 0,
    height: 1,
    backgroundColor: "#E0E6ED"
  },
  faculties: {
    paddingVertical: 8,
    paddingHorizontal: 0
  },
  facultiesH3: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
  },
  facCategories: {
    paddingVertical: 8,
    paddingHorizontal: 0
  },
  facCategoriesH4: {
    ...fontWeight.BOLD,
  },
  facCategoriesUl: {
    marginTop: 0,
    marginRight: 16,
    marginBottom: 8,
    marginVertical: 16
  },
  facilities: {
    paddingVertical: 8
  },
  facilitiesH3: {
    ...fontSize.LARGE,
    ...fontWeight.BOLD
  },
  facilitiesList: {
    padding: 4,
    color: "#333333",
    ...fontWeight.REGULAR
  },
  album: {
    paddingVertical: 8
  },
  albumH3: {
    paddingBottom: 8,
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
  },
  galleries: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  thumbnail: {
    width: "48%",
    paddingRight: 8,
    overflow: "hidden",
    display: "flex",
    justifyContent: "flex-start",
    textAlign: "left",
    borderRadius: 3
  },
  thumbnailImg: {
    // flex: 1,
  },
  gallery: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    width: "50%"
  },
  galleryItem: {
    width: "50%",
    paddingLeft: 8,
    paddingBottom: 8,
    position: "relative"
  },
  galleryItemImg: {
    width: "100%",
    borderRadius: 3
  },
  galleryMore: {
    position: "absolute",
    top: "25%",
    left: "35%",
    color: "white",
    ...fontWeight.SEMIBOLD,
    ...fontSize.XLARGE
  },
  fullMode: {
    resizeMode: "cover",
    flex: 1,
    backgroundColor: "#333333"
  },
  backIcon: {
    margin: 16,
    width: 24,
    height: 24
  },
  scholar: {
    paddingVertical: 8,
    ...fontWeight.BOLD,
  },
  scholarH3: {
    paddingBottom: 8,
    ...fontSize.LARGE,
    ...fontWeight.BOLD,
  },
  scholar_row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: 4
  },
  scholar_img: {
    paddingRight: 8,
    minWidth: 108
  }

  // .reviews {
  //   padding: 8px 0;

  //   h3 {
  //     padding: 8px 0;
  //     font-size: 1.6rem;
  //     font-weight: 600;
  //   }
  //   h4 {
  //     color: var(--color-white-smoke);
  //     padding-bottom: 12px;
  //   }
  // }

  // .input_review {
  //   display: flex;
  //   align-items: center;
  //   background-color: var(--color-gray-lighter);
  //   padding: 8px;
  //   border-radius: 3px;

  //   img {
  //     height: 16px;
  //     padding-right: 8px;
  //   }
  //   input {
  //     border: none;
  //     width: 100%;
  //     background-color: var(--color-gray-lighter);
  //   }
  // }

  // .review_list {
  //   padding: 8px 0;
  // }

  // .review_row {
  //   display: flex;
  //   padding: 8px 0;

  //   img {
  //     height: 40px;
  //     padding-right: 8px;
  //     border-radius: 3px;
  //   }

  //   .review_wrap {
  //     width: calc(100% - 48px);
  //     .username {
  //       font-weight: 600;
  //     }
  //     .review_rating {
  //       display: flex;
  //       justify-content: space-between;
  //       color: var(--color-white-smoke);

  //       .rating_number {
  //         font-weight: 600;
  //         color: var(--color-black);
  //       }
  //     }
  //     .review_content {
  //       padding: 8px 0;
  //     }
  //   }
});

export default styles;
