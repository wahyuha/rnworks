import _ from "lodash";
import { Dimensions, View, Text, TouchableOpacity } from "react-native";
import { pushTo } from "@navigations";
import { NEWS } from "@navigations/screens";
import AutoHeightImage from "react-native-auto-height-image";
import React from "react";
import styles from "./styles";

const { width: SCREEN_WIDTH } = Dimensions.get("window");

const News = props => {
  const { detail, componentId } = props;
  const { news } = detail;

  const NewsItem = detail => {
    const { image } = detail;
    return (
      <TouchableOpacity
        onPress={() =>
          pushTo(componentId, NEWS, {
            props: { detail },
            topBar: { visible: true, title: detail.title }
          })
        }
      >
        <View style={styles.scholar_row}>
          <View style={styles.scholar_img}>
            {image && (
              <AutoHeightImage
                source={{ uri: image.url }}
                // style={styles.galleryItemImg}
                width={SCREEN_WIDTH / 3}
              />
            )}
          </View>
          <Text style={styles.ctc_text}>{detail.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  if (_.isEmpty(news)) return null;
  return (
    <>
      <View style={styles.scholar}>
        <Text style={styles.scholarH3}>Campus Update</Text>
        {news.map(value => (
          <NewsItem {...value} />
        ))}
      </View>
      <View style={styles.borders} />
    </>
  );
};

export default News;
