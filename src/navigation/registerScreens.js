import { Navigation } from "react-native-navigation";
import Provider from "../utils/appStoreProvider";
import React from "react";
import Routes from "./routes";

import GalleryFull from "@components/DetailUniversity/galleryFull";

const WrappedComponent = Component => props => {
  const EnhancedComponent = () => (
    <Provider>
      <Component {...props} />
    </Provider>
  );

  return <EnhancedComponent />;
};

export default function() {
  Object.keys(Routes).map(key =>
    Navigation.registerComponent(key, () =>
      WrappedComponent(Routes[key].screen)
    )
  );

  // TODO : put in the object
  Navigation.registerComponent("detail.top", () => Routes["DETAIL_SCREEN"].topBar);
  Navigation.registerComponent("search", () => Routes["SEARCH"].screen);
  Navigation.registerComponent("gallery.full", () => GalleryFull);
}
