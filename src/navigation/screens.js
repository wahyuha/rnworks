export const HOME_SCREEN = "HOME_SCREEN";
export const CITY_CATEGORIES = "CITY_CATEGORIES";
export const LIST_UNIVERSITY = "LIST_UNIVERSITY";
export const DETAIL_SCREEN = "DETAIL_SCREEN";
export const SCHOLARSHIPS = "SCHOLARSHIPS";
export const NEWS = "NEWS";
export const SEARCH = "SEARCH";
export const SETTINGS = "SETTINGS";
