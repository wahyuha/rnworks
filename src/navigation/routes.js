import Home from "@screens/home";
import CityCategories from "@screens/cityCategories";
import ListingUniversity from "@screens/listingUniversity";
import DetailUniversity from "@screens/detailUniversity";
import Scholarships from "@screens/scholarships";
import News from "@screens/news";
import Settings from "@screens/settings";
import SearchModal from '@screens/search';

import DetailTop from '@components/Header/navigation';

import {
  HOME_SCREEN,
  CITY_CATEGORIES,
  LIST_UNIVERSITY,
  DETAIL_SCREEN,
  SCHOLARSHIPS,
  SEARCH,
  NEWS,
  SETTINGS
} from "./screens";

const Routes = {
  HOME_SCREEN: {
    name: HOME_SCREEN,
    label: "Home",
    screen: Home
  },
  CITY_CATEGORIES: {
    name: CITY_CATEGORIES,
    screen: CityCategories
  },
  LIST_UNIVERSITY: {
    name: LIST_UNIVERSITY,
    screen: ListingUniversity
  },
  DETAIL_SCREEN: {
    name: DETAIL_SCREEN,
    screen: DetailUniversity,
    topBar: DetailTop
  },
  SCHOLARSHIPS: {
    name: SCHOLARSHIPS,
    screen: Scholarships
  },
  SEARCH: {
    name: SEARCH,
    screen: SearchModal
  },
  NEWS: {
    name: NEWS,
    screen: News
  },
  SETTINGS: {
    name: SETTINGS,
    screen: Settings
  }
};

export default Routes;
