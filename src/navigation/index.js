import { Navigation } from "react-native-navigation";
import { HOME_SCREEN, SCHOLARSHIPS, NEWS, SETTINGS } from "./screens";
import Routes from "./routes";

export function pushToStackRoot(componentId, PageScreen, options = {}) {
  Navigation.setStackRoot(componentId, [
    {
      component: {
        name: PageScreen,
        options: {
          topBar: {
            visible: false
          },
          sideMenu: {
            left: {
              enabled: false
            }
          },
          ...options
        }
      }
    }
  ]);
}

export function pushToRoot(PageScreen, title, options = {}) {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: PageScreen,
              options: {
                topBar: {
                  visible: false,
                  title: {
                    text: title
                  }
                },
                ...options
              }
            }
          }
        ]
      }
    }
  });
}

export function pushTo(componentId, PageScreen, options = { props: {} }) {
  Navigation.push(componentId, {
    component: {
      name: PageScreen,
      passProps: { ...options.props },
      options: {
        topBar: {
          visible: (options.topBar && options.topBar.visible) || false
          // title: {
          //   text: PageScreen
          // }
        },
        bottomTabs: {
          visible: false,
          drawBehind: true
        },
        layout: {
          orientation: ["portrait", "landscape"]
        },
        animations: {
          push: {
            content: {
              x: {
                from: 1000,
                to: 0,
                duration: 400,
                interpolation: "accelerate"
              }
            }
          },
          pop: {
            content: {
              x: {
                from: 0,
                to: 1000,
                duration: 200,
                interpolation: "accelerate"
              }
            }
          }
        },
        ...options // TODO: not here
      }
    }
  });
}

export function backToPrevious(componentId) {
  Navigation.pop(componentId);
}

export const showModal = (PageScreen, options = { props: {} }) => {
  Navigation.showModal({
    stack: {
      children: [
        {
          component: {
            name: PageScreen,
            passProps: { ...options.props },
            options: {
              topBar: {
                visible: false
              },
              ...options // TODO: not here
            }
          }
        }
      ]
    }
  });
};

export function dismissModalAll() {
  Navigation.dismissAllModals();
}

export function pushToMainScreen() {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: "MAIN",
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: HOME_SCREEN,
                    options: {
                      topBar: {
                        visible: false
                      },
                      bottomTab: {
                        textColor: "#6A6B6F",
                        iconColor: "#6A6B6F",
                        selectedIconColor: "#60B2A4",
                        selectedTextColor: "#60B2A4",
                        text: Routes[HOME_SCREEN].label,
                        icon: require("../../static/icons/home.png")
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            component: {
              name: SCHOLARSHIPS,
              options: {
                bottomTab: {
                  textColor: "#6A6B6F",
                  iconColor: "#6A6B6F",
                  selectedIconColor: "#60B2A4",
                  selectedTextColor: "#60B2A4",
                  text: "Scholarship",
                  icon: require("../../static/icons/profile.png")
                }
              }
            }
          },
          {
            component: {
              name: NEWS,
              options: {
                bottomTab: {
                  textColor: "#6A6B6F",
                  iconColor: "#6A6B6F",
                  selectedIconColor: "#60B2A4",
                  selectedTextColor: "#60B2A4",
                  text: "News",
                  icon: require("../../static/icons/agreement.png")
                }
              }
            }
          },
          {
            component: {
              name: SETTINGS,
              options: {
                bottomTab: {
                  textColor: "#6A6B6F",
                  iconColor: "#6A6B6F",
                  selectedIconColor: "#60B2A4",
                  selectedTextColor: "#60B2A4",
                  text: "Settings",
                  icon: require("../../static/icons/setting.png")
                }
              }
            }
          }
        ]
      }
    }
  });
}
