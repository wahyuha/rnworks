module.exports = {
  presets: [
    "module:metro-react-native-babel-preset",
    "module:react-native-dotenv"
  ],
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          "@screens": "./src/screens/",
          "@pages": "./src/pages/",
          "@navigations": "./src/navigation/",
          "@components": "./src/components/",
          "@styles": "./src/styles",
          "@utils": "./src/utils/",
          "@redux": "./src/redux/",
          "@config": "./config/",
          "@images": "./static/images/",
          "@icons": "./static/icons/",
          "@uikit": "./src/uikit"
        }
      }
    ]
  ]
};
